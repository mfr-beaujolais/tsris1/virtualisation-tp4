FROM debian:bookworm-slim

###
# Upgrade the system
###
RUN apt-get update --quiet --quiet \
    && apt-get upgrade --quiet --quiet

RUN apt-get install --quiet --quiet --yes \
    --no-install-recommends --no-install-suggests \
    ca-certificates libsqlite3-dev sqlite3 unzip wget

## Installation
RUN mkdir /app
WORKDIR /app
RUN wget -O gophish.zip https://github.com/gophish/gophish/releases/download/v0.12.1/gophish-v0.12.1-linux-64bit.zip
RUN unzip gophish.zip
RUN rm gophish.zip
## Rendre executable !
RUN chmod +x gophish

## Run
EXPOSE 3333/TCP 80/TCP
ENTRYPOINT ["./gophish"]